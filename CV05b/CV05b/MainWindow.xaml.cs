﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CV06b
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static List<Pes> list_psu = new List<Pes>();
        public MainWindow()
        {
            InitializeComponent();
        }

        public void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            foreach (var o in list_psu) { lbox.Items.Add(o.jmeno); }
            /*List<string> listStringu = new List<string>(new string[] { "Jedna", "dva", "tri", "ctyri", "pet", "sest", "sedm", "osm", "devet", "10" });
             foreach (var o in listStringu){lbox.Items.Add(o);}*/
        }
        public void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            //lbox.Items.Remove(lbox.SelectedItem); 
            lbox.Items.RemoveAt(lbox.Items.Count - 1);
            //lbox.Items.Clear();
        }

        private void btnText_Click(object sender, RoutedEventArgs e)
        {
            lbox.Items.Add(txtbox.Text);
        }
        private void reloadListBox()
        {
            foreach (var o in list_psu) { if (!lbox.Items.Contains(o.jmeno)) { lbox.Items.Add(o.jmeno); lbox.Items.Add(o.vek); lbox.Items.Add(o.rasa); } }
        }

        private void btn_change_Click(object sender, RoutedEventArgs e)
        {
            Okno2 subWindow = new Okno2();
            subWindow.ShowDialog();
            /*string test = subWindow.tbJmeno.Text;
            testlabel.Content = test;*/
            list_psu.Add(new Pes(subWindow.tbJmeno.Text, Int32.Parse(subWindow.tbVek.Text), subWindow.tbRasa.Text));
            reloadListBox();
        }
       
    }
}
