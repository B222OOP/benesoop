﻿using CV08Part2;
using Microsoft.VisualBasic;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static System.Net.Mime.MediaTypeNames;


namespace Videopujcovna
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        public Uzivatel uzivatel;
        bool kontrola=false;
        public int kdo_pouziva;
        int ID_rezervace = 1;
        public MainWindow()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();
        }

       

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
        //TODO:registrace uzivatele
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            reader = dbconn.Select("SELECT * FROM Uzivatel");
            while(reader.Read())
            {
                if (reader.GetString(3) == tbox1.Text && reader.GetString(4) == tbox2.Text)
                {
                    this.uzivatel = new Uzivatel(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetInt32(5));
                    if (reader.GetInt32(5) == 1)
                    {
                        while (true)
                        {
                            Admin okno1 = new Admin();
                            //okno2.test.Content = $"{uzivatel.ID_uzivatel} {DateTime.Today.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss")}";
                            okno1.ShowDialog();
                            reader = dbconn.Select("SELECT * FROM Rezervace");
                            while (reader.Read() && ID_rezervace.Equals(reader.GetInt32(0)))
                                ID_rezervace++;
                            if (okno1.rezervace == true)
                            {
                                string ins = (String.Format("INSERT INTO `Rezervace`(`ID_rezervace`, `Uzivatel`, `Film`, `Datum`) VALUES({0},{1},{2},\"{3}\")", ID_rezervace, uzivatel.ID_uzivatel, okno1.film[okno1.lbox1.SelectedIndex].ID_film, DateTime.Today.AddDays(7).ToString("yyyy-MM-dd H:mm:ss")));
                                this.dbconn.Insert(ins);
                                okno1.rezervace = false;
                            }
                            if (okno1.uprava == true)
                            {
                                Upravit_profil okno = new Upravit_profil();
                                okno.jmeno.Text = uzivatel.jmeno;
                                okno.prijmeni.Text = uzivatel.prijmeni;
                                okno.prezdivka.Text = uzivatel.prezdivka;
                                okno.heslo.Text = uzivatel.heslo;
                                okno.ShowDialog();
                                if (
                                okno.jmeno.Text == uzivatel.jmeno &&
                                okno.prijmeni.Text == uzivatel.prijmeni &&
                                okno.prezdivka.Text == uzivatel.prezdivka &&
                                okno.heslo.Text == uzivatel.heslo)
                                {
                                    MessageBox.Show("Neprovedena žádná změna.");
                                }
                                else
                                {
                                    Uzivatel upraveny = okno.uzivatel;
                                    upraveny.ID_uzivatel = uzivatel.ID_uzivatel;
                                    upraveny.role = uzivatel.role;
                                    this.dbconn.Update(String.Format("UPDATE `Uzivatel` SET `Jmeno` = '{0}', `Prijmeni` = '{1}', `Prezdivka` = '{2}', `Heslo` = '{3}' WHERE `Uzivatel`.`ID_uzivatel` = {4};", upraveny.jmeno, upraveny.prijmeni, upraveny.prezdivka, upraveny.heslo, upraveny.ID_uzivatel));
                                    okno1.uprava = false;
                                    this.uzivatel = new Uzivatel(upraveny.ID_uzivatel, upraveny.jmeno, upraveny.prijmeni, upraveny.prezdivka, upraveny.heslo, upraveny.role);
                                }
                            }
                            if (okno1.zavrit == true)
                                
                            {
                                okno1.lbox1.SelectedIndex = 0;
                                break; }
                        }

                    }
                    else if (reader.GetInt32(5) == 2)
                    {
                        while(true)
                        { 
                        Zakaznik okno2 = new Zakaznik();
                        //okno2.test.Content = $"{uzivatel.ID_uzivatel} {DateTime.Today.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss")}";
                        okno2.ShowDialog();
                            reader = dbconn.Select("SELECT * FROM Rezervace");
                            while (reader.Read() && ID_rezervace.Equals(reader.GetInt32(0)))
                                ID_rezervace++;
                            if (okno2.rezervace == true)
                        {
                            string ins = (String.Format("INSERT INTO `Rezervace`(`ID_rezervace`, `Uzivatel`, `Film`, `Datum`) VALUES({0},{1},{2},\"{3}\")", ID_rezervace, uzivatel.ID_uzivatel, okno2.film[okno2.lbox1.SelectedIndex].ID_film, DateTime.Today.AddDays(7).ToString("yyyy-MM-dd H:mm:ss")));
                            this.dbconn.Insert(ins);
                            okno2.rezervace = false;
                        }
                        if (okno2.uprava == true)
                        {
                            Upravit_profil okno = new Upravit_profil();
                            okno.jmeno.Text = uzivatel.jmeno;
                            okno.prijmeni.Text = uzivatel.prijmeni;
                            okno.prezdivka.Text = uzivatel.prezdivka;
                            okno.heslo.Text = uzivatel.heslo;
                            okno.ShowDialog();
                            if (
                            okno.jmeno.Text == uzivatel.jmeno &&
                            okno.prijmeni.Text == uzivatel.prijmeni &&
                            okno.prezdivka.Text == uzivatel.prezdivka &&
                            okno.heslo.Text == uzivatel.heslo)
                            {
                                MessageBox.Show("Neprovedena žádná změna.");
                            }
                            else
                            {
                                Uzivatel upraveny = okno.uzivatel;
                                upraveny.ID_uzivatel = uzivatel.ID_uzivatel;
                                upraveny.role = uzivatel.role;
                                this.dbconn.Update(String.Format("UPDATE `Uzivatel` SET `Jmeno` = '{0}', `Prijmeni` = '{1}', `Prezdivka` = '{2}', `Heslo` = '{3}' WHERE `Uzivatel`.`ID_uzivatel` = {4};", upraveny.jmeno, upraveny.prijmeni, upraveny.prezdivka, upraveny.heslo, upraveny.ID_uzivatel));
                                okno2.uprava = false;
                                    this.uzivatel = new Uzivatel(upraveny.ID_uzivatel, upraveny.jmeno, upraveny.prijmeni, upraveny.prezdivka, upraveny.heslo, upraveny.role);
                            }
                        }
                        if(okno2.zavrit== true)
                            { okno2.lbox1.SelectedIndex = 0; break; }
                        }
                    }
                    kontrola = true;
                }
            }
            if (kontrola==false)
            { MessageBox.Show("Špatně zadané jméno nebo heslo"); }
            

                
        }
        
    }
}
