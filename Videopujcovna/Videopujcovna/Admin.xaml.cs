﻿using CV08Part2;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Videopujcovna
{
    /// <summary>
    /// Interaction logic for Admin.xaml
    /// </summary>
    public partial class Admin : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        public List<Film> film = new List<Film>();
        public List<Uzivatel> uzivatel = new List<Uzivatel>();
        string sel1 = "SELECT * FROM Film";
        string sel2 = "SELECT * FROM Uzivatel";
        public bool rezervace = false;
        public bool uprava = false;
        public bool zavrit = false;

        public Admin()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();
            refresh_movies();
            refresh_users();
            //test.Content = $"{DateTime.Today.ToString("D")}";

        }


        public void refresh_movies()
        {
            if (film.Count > 0 )
            {
                film.Clear();
                lbox1.Items.Clear();    
            }
            reader = this.dbconn.Select(sel1);
            while (reader.Read())
            {
                film.Add(new Film(
                reader.GetInt32(0),
                reader.GetString(1),
                reader.GetString(2),
                reader.GetBoolean(3)
                    ));
            }
            this.dbconn.CloseConn();

            foreach (var o in this.film)
            {
                lbox1.Items.Add(String.Format("{0}", o.nazev));
            }
        }
        public void refresh_users()
        {
            if (uzivatel.Count > 0)
            {
                uzivatel.Clear();
                lbox3.Items.Clear();
            }
            
            reader = this.dbconn.Select(sel2);
            while (reader.Read())
            {
                uzivatel.Add(new Uzivatel(
                reader.GetInt32(0),
                reader.GetString(1),
                reader.GetString(2),
                reader.GetString(3),
                reader.GetString(4),
                reader.GetInt32(5)
                    ));
            }

            this.dbconn.CloseConn();

            foreach (var o in this.uzivatel)
            {
                lbox3.Items.Add(String.Format("{0}  {1}  ({2})", o.jmeno, o.prijmeni, o.prezdivka));
            }

        }
        public DateTime datum()
        {
            sel1 = (String.Format("SELECT Datum FROM Rezervace WHERE Film={0}", film[lbox1.SelectedIndex].ID_film));
            reader = this.dbconn.Select(sel1);
            reader.Read();
            return reader.GetDateTime(0);

        }
        public void info(object sender, SelectionChangedEventArgs args)
        {
            if (film.Count > 0)
            {
                lbox2.Items.Clear();
                //test.Content = $"{DateTime.Today.AddDays(7).ToString()}";
                if (film[lbox1.SelectedIndex].rezervovany == true)
                {

                    lbox2.Items.Add(String.Format("Film je rezervovany do: {0}", datum()));
                }
                else
                {
                    lbox2.Items.Add(String.Format("{0}", film[lbox1.SelectedIndex].zanr));
                }
                lbox2.Visibility = Visibility.Visible;
                inf.Visibility = Visibility.Visible;
            }
            MainWindow okno = new MainWindow();
            Uzivatel novy = okno.uzivatel;
            //reader = dbconn.Select(String.Format("SELECT ID_uzivatel FROM Uzivatel WHERE Prezdivka=\"{0}\" AND Heslo=\"{1}\"", okno.tbox1.Text, okno.tbox2.Text));
            //reader.Read();

        }
        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            lbox2.Items.Clear();
            sel1 = "SELECT * FROM Film ORDER BY Nazev ASC";
            refresh_movies();
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            lbox2.Items.Clear();
            sel1 = "SELECT * FROM Film join Rezervace on Rezervace.Film=Film.ID_film WHERE Rezervovany=1 ORDER BY Rezervace.Datum ASC";
            refresh_movies();
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            lbox2.Items.Clear();
            Zanr okno = new Zanr();
            okno.ShowDialog();
            sel1 = (String.Format("SELECT * FROM Film WHERE Zanr=\"{0}\"", okno.box_zanr.Text));
            refresh_movies();

        }

        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            Rezervace okno = new Rezervace();
            okno.ShowDialog();
        }

        private void btn5_Click(object sender, RoutedEventArgs e)
        {
            Upravit_profil okno = new Upravit_profil();
            okno.jmeno.Text = uzivatel[lbox3.SelectedIndex].jmeno;
            okno.prijmeni.Text = uzivatel[lbox3.SelectedIndex].prijmeni;
            okno.prezdivka.Text = uzivatel[lbox3.SelectedIndex].prezdivka;
            okno.heslo.Text = uzivatel[lbox3.SelectedIndex].heslo;
            okno.ShowDialog();
            if (
            okno.jmeno.Text == uzivatel[lbox3.SelectedIndex].jmeno &&
            okno.prijmeni.Text == uzivatel[lbox3.SelectedIndex].prijmeni &&
            okno.prezdivka.Text == uzivatel[lbox3.SelectedIndex].prezdivka &&
            okno.heslo.Text == uzivatel[lbox3.SelectedIndex].heslo)
            {
                MessageBox.Show("Neprovedena žádná změna.");
            }
            else
            {
                Uzivatel upraveny = okno.uzivatel;
                upraveny.ID_uzivatel = uzivatel[lbox3.SelectedIndex].ID_uzivatel;
                upraveny.role = uzivatel[lbox3.SelectedIndex].role;
                this.dbconn.Update(String.Format("UPDATE `Uzivatel` SET `Jmeno` = '{0}', `Prijmeni` = '{1}', `Prezdivka` = '{2}', `Heslo` = '{3}' WHERE `Uzivatel`.`ID_uzivatel` = {4};", upraveny.jmeno, upraveny.prijmeni, upraveny.prezdivka, upraveny.heslo, upraveny.ID_uzivatel));
                this.uzivatel[lbox3.SelectedIndex] = new Uzivatel(upraveny.ID_uzivatel, upraveny.jmeno, upraveny.prijmeni, upraveny.prezdivka, upraveny.heslo, upraveny.role);
                refresh_users();
            }


        }

        private void btn6_Click(object sender, RoutedEventArgs e)
        {
            zavrit = true;
            Close();
        }

        private void btn7_Click(object sender, RoutedEventArgs e)
        {
            lbox3.Items.Clear();
            sel2 = "SELECT * FROM Uzivatel ORDER BY Jmeno";
            refresh_users();
        }

        private void btn8_Click(object sender, RoutedEventArgs e)
        {
            lbox3.Items.Clear();
            sel2 = "SELECT * FROM Uzivatel ORDER BY Prijmeni";
            refresh_users();
        }

        private void btn9_Click(object sender, RoutedEventArgs e)
        {
            lbox3.Items.Clear();
            sel2 = "SELECT * FROM Uzivatel ORDER BY Prezdivka";
            refresh_users();
        }

        private void btn10_Click(object sender, RoutedEventArgs e)
        {
            Pridat_uziv okno = new Pridat_uziv();
            okno.ShowDialog();
            Uzivatel novy = okno.uzivatel;
            string sel = "SELECT * FROM Uzivatel";
            reader = this.dbconn.Select(sel);
            while (reader.Read() && novy.ID_uzivatel.Equals(reader.GetInt32(0)))
                novy.ID_uzivatel++;
            
            if (!lbox3.Items.Contains(novy.prezdivka))
            {

                try
                {
                    dbconn.Insert(String.Format("INSERT into `Uzivatel` (`ID_uzivatel`, `Jmeno`, `Prijmeni`, `Prezdivka`, Heslo,`Role`) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')", novy.ID_uzivatel, novy.jmeno, novy.prijmeni, novy.prezdivka, novy.heslo, novy.role));
                }
                catch (MySqlConnector.MySqlException)
                { MessageBox.Show("Zadana role neexistuje."); ; }
            }
            else { MessageBox.Show("Uživatel již existuje. Zkuste zadat uživatele znovu."); }
            refresh_users();
        }

        private void btn11_Click(object sender, RoutedEventArgs e)
        {
            dbconn.Delete(String.Format("DELETE FROM `Uzivatel` WHERE `Uzivatel`.`ID_uzivatel` = {0}", uzivatel[lbox3.SelectedIndex].ID_uzivatel));
            refresh_users();
        }
    }
}
