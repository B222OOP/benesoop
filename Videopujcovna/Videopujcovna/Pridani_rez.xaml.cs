﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Videopujcovna
{
    /// <summary>
    /// Interaction logic for Pridani_rez.xaml
    /// </summary>
    public partial class Pridani_rez : Window
    {
        public Rezervace_trida rezervace { get; set; }
        int id = 1;
        public Pridani_rez()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.rezervace = new Rezervace_trida(id, Int32.Parse(uzivatel.Text), Int32.Parse(film.Text), DateTime.Parse(datum.Text));
            Close();
        }
    }
}
