﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Videopujcovna
{
    /// <summary>
    /// Interaction logic for Upravit_profil.xaml
    /// </summary>
    public partial class Upravit_profil : Window
    {
        public Uzivatel uzivatel { get; set; }
        int id = 1;
        public Upravit_profil()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.uzivatel = new Uzivatel(id, jmeno.Text, prijmeni.Text, prezdivka.Text, heslo.Text,id);
            Close();
        }
    }
}
