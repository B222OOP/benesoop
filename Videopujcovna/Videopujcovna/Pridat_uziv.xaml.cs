﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Videopujcovna
{
    /// <summary>
    /// Interaction logic for Pridat_uziv.xaml
    /// </summary>
    public partial class Pridat_uziv : Window
    {
        public Uzivatel uzivatel { get; set; }
        int id = 1;
        public Pridat_uziv()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.uzivatel = new Uzivatel(id, jmeno.Text, prijmeni.Text, prezdivka.Text, heslo.Text, Int32.Parse(role.Text));
            Close();
        }
    }
}
