﻿using System;
using System.Collections.Generic;
using System.Reflection.PortableExecutable;
using System.Text;

namespace Videopujcovna
{
    public class Rezervace_trida
    {
        public int ID_rezervace { get; set; }
        public int uzivatel { get; set; }
        public int film { get; set; }
        public DateTime datum { get; set; }
        public Rezervace_trida(int ID_rezervace, int uzivatel, int film, DateTime datum)
        {
            this.ID_rezervace = ID_rezervace; 
            this.uzivatel = uzivatel;
            this.film = film;
            this.datum = datum;
        }
    }
}
