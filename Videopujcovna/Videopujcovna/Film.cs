﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Videopujcovna
{
    public class Film
    {
        public int ID_film { get; set; }
        public string nazev { get; set; }
        public string zanr { get; set; }
        public bool rezervovany { get; set; }
        public Film(int ID_film, string nazev, string zanr, bool rezervovany)
        {
            this.ID_film = ID_film;
            this.nazev = nazev;
            this.zanr = zanr;
            this.rezervovany = rezervovany;
        }
    }
}
