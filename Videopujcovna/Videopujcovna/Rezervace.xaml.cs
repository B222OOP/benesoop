﻿using CV08Part2;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Videopujcovna
{
    /// <summary>
    /// Interaction logic for Rezervace.xaml
    /// </summary>
    public partial class Rezervace : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        public List<Rezervace_trida> rezervace = new List<Rezervace_trida>();
        string sel = "SELECT * FROM Rezervace";
        
        public Rezervace()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();
            refresh();
        }
        public void refresh()
        {
            if (rezervace.Count > 0)
            {
                rezervace.Clear();
                lbox1.Items.Clear();
            }
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                rezervace.Add(new Rezervace_trida(
                reader.GetInt32(0),
                reader.GetInt32(1),
                reader.GetInt32(2),
                reader.GetDateTime(3)
                    ));
            }

            this.dbconn.CloseConn();

            foreach (var o in this.rezervace)
            {
                reader = dbconn.Select(String.Format("SELECT Jmeno, Prijmeni FROM `Uzivatel` join Rezervace on Rezervace.Uzivatel=Uzivatel.ID_uzivatel WHERE Uzivatel.ID_uzivatel={0}",o.uzivatel));
                reader.Read();
                string uzivatel = String.Format("{0} {1}", reader.GetString(0), reader.GetString(1));
                reader = dbconn.Select(String.Format("SELECT Nazev FROM `Film` join Rezervace on Rezervace.Film=Film.ID_film WHERE Film.ID_film={0}",o.film));
                reader.Read();
                string film = String.Format("{0}", reader.GetString(0));
                lbox1.Items.Add(String.Format("{0}  {1}  {2} ", uzivatel,film,o.datum));
                uzivatel = "";
                film = "";
                this.dbconn.CloseConn();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {

        }
        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            Pridani_rez okno = new Pridani_rez();
            okno.ShowDialog();
            Rezervace_trida novy = okno.rezervace;
            string sel = "SELECT * FROM Rezervace";
            reader = this.dbconn.Select(sel);
            while (reader.Read() && novy.ID_rezervace.Equals(reader.GetInt32(0)))
                novy.ID_rezervace++;

            if (reader.GetInt32(2)!=novy.film)
            {

                try
                {
                    dbconn.Insert(String.Format("INSERT into `Rezervace` (`ID_rezervace`, `Uzivatel`, `Film`, `datum`) VALUES ('{0}', '{1}', '{2}', '{3}')", novy.ID_rezervace, novy.uzivatel, novy.film, novy.datum));
                }
                catch (MySqlConnector.MySqlException)
                { MessageBox.Show("Nelze vložit"); ; }
            }
            else { MessageBox.Show("Zvolený film už je registrovaný. Zkuste zadat jiný film."); }
            refresh();
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            dbconn.Delete(String.Format("DELETE FROM `Rezervace` WHERE `Rezervace`.`ID_rezervace` = {0}", rezervace[lbox1.SelectedIndex].ID_rezervace));
            refresh();
        }
    }
}
