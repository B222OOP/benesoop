﻿using CV08Part2;
using Microsoft.VisualBasic;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Videopujcovna
{
    /// <summary>
    /// Interaction logic for Zakaznik.xaml
    /// </summary>
    public partial class Zakaznik : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        public List<Film> film = new List<Film>();                
        string sel = "SELECT * FROM Film";
        public bool rezervace = false;
        public bool uprava = false;
        public bool zavrit = false;

        public Zakaznik()
        {  
            this.dbconn = new DB_connect();
            InitializeComponent();
            refresh();
            //test.Content = $"{DateTime.Today.ToString("D")}";
           
        }

      
        public void refresh()
        {
            if(film.Count > 0)
            {
                film.Clear();
                lbox1.Items.Clear();
            }
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                film.Add(new Film(
                reader.GetInt32(0),
                reader.GetString(1),
                reader.GetString(2),
                reader.GetBoolean(3)
                    ));
            }
            
            this.dbconn.CloseConn();

            foreach (var o in this.film)
            {
                lbox1.Items.Add(String.Format("{0}", o.nazev ));
            }
            
        }
        public DateTime datum()
        {
            sel = (String.Format("SELECT Datum FROM Rezervace WHERE Film={0}", film[lbox1.SelectedIndex].ID_film));
            reader = this.dbconn.Select(sel);
            reader.Read();
            return reader.GetDateTime(0);
          
        }
        public void info(object sender, SelectionChangedEventArgs args)
        {
            if (film.Count > 0)
            {
                lbox2.Items.Clear();
                //test.Content = $"{DateTime.Today.AddDays(7).ToString()}";
                if (film[lbox1.SelectedIndex].rezervovany == true)
                {
                    
                    lbox2.Items.Add(String.Format("Film je rezervovany do: {0}", datum()));
                }
                else
                {
                    lbox2.Items.Add(String.Format("{0}", film[lbox1.SelectedIndex].zanr));
                }
                lbox2.Visibility = Visibility.Visible;
                inf.Visibility = Visibility.Visible;
            }
            MainWindow okno = new MainWindow();
            Uzivatel novy = okno.uzivatel;
            //reader = dbconn.Select(String.Format("SELECT ID_uzivatel FROM Uzivatel WHERE Prezdivka=\"{0}\" AND Heslo=\"{1}\"", okno.tbox1.Text, okno.tbox2.Text));
            //reader.Read();
            
        }
        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            lbox2.Items.Clear();
            sel = "SELECT * FROM Film ORDER BY Nazev ASC";
            refresh();
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            lbox2.Items.Clear();
            sel = "SELECT * FROM Film join Rezervace on Rezervace.Film=Film.ID_film WHERE Rezervovany=1 ORDER BY Rezervace.Datum ASC";
            refresh();
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            lbox2.Items.Clear();
            Zanr okno = new Zanr();
            okno.ShowDialog();
            sel = (String.Format("SELECT * FROM Film WHERE Zanr=\"{0}\"", okno.box_zanr.Text));
            refresh();
            
        }

        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            film[lbox1.SelectedIndex].rezervovany = true;
            lbox2.Items.Clear();
            lbox2.Items.Add(String.Format("Film je rezervovany do: {0}", DateTime.Today.AddDays(7)));
            string upd = (String.Format("UPDATE `Film` SET `Rezervovany` = '1' WHERE `Film`.`ID_film` = {0}", lbox1.SelectedIndex + 1));
            rezervace = true;
            //MainWindow okno = new MainWindow();
            //reader = dbconn.Select(String.Format("SELECT ID_uzivatel FROM Uzivatel WHERE Prezdivka={0} AND Heslo={1}",okno.tbox1.Text, okno.tbox2.Text));
            //reader.Read();
            //string ins =(String.Format("INSERT INTO `Rezervace`(`ID_rezervace`, `Uzivatel`, `Film`, `Datum`) VALUES(NULL,{0},{1},{2}",reader.GetInt32(0), film[lbox1.SelectedIndex].ID_film, DateTime.Today.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss")));
            this.dbconn.Update(upd);
            this.dbconn.CloseConn();
            Close();
        }

        private void btn5_Click(object sender, RoutedEventArgs e)
        {
            uprava = true;
            Close();
        }

        private void btn6_Click(object sender, RoutedEventArgs e)
        {
            zavrit = true;
            Close();
        }
    }
}
