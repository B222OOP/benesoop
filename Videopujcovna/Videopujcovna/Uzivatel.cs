﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Videopujcovna
{
    public class Uzivatel
    {
        public int ID_uzivatel { get; set; }
        public string jmeno { get; set; }
        public string prijmeni { get; set; }
        public string prezdivka { get; set; }
        public string heslo { get; set; }
        public int role { get; set; }
        public Uzivatel(int ID_uzivatel, string jmeno, string prijmeni, string prezdivka, string heslo, int role)
        {
            this.ID_uzivatel = ID_uzivatel;
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.prezdivka = prezdivka;
            this.heslo=heslo;
            this.role = role;
        }
        
    }
}
