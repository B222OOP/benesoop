﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CV08Part1;
using MySqlConnector;

namespace CV08Part2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        public List<Patient> pacienti = new List<Patient>();
        public MainWindow()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();
            refresh();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            ADD okno = new ADD();
            okno.ShowDialog();
            Patient novy = okno.pacient;
            string sel = "SELECT * FROM Pacient";
            reader = this.dbconn.Select(sel);
            while (reader.Read() && novy.ID_pacient.Equals(reader.GetInt32(0)))
                novy.ID_pacient++;
            pacienti.Add(novy);
            if (!lbox.Items.Contains(novy.jmeno_p + " " + novy.prijmeni_p))
            {
                lbox.Items.Add(novy.jmeno_p + " " + novy.prijmeni_p);
                try
                {
                    dbconn.Insert(String.Format("INSERT into `Pacient` (`ID_pacient`, `jmeno_p`, `prijmeni_p`, `vek`, `adresa_ID`) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", novy.ID_pacient, novy.jmeno_p, novy.prijmeni_p, novy.vek, novy.adresa_ID));
                }
                catch (MySqlConnector.MySqlException)
                { MessageBox.Show("Zadana adresa neexistuje."); ; }
            }
            else MessageBox.Show("Pacient již existuje. Zkuste zadat pacienta znovu.");
            lbox.Items.Clear();
            refresh();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            
            if (lbox.SelectedItem == null)
            { MessageBox.Show("Označte pacienta, kterého chcete odstranit."); }
            else
            {
                string[] s = lbox.SelectedItem.ToString().Split(' ');
                Patient pat = getByName(s[0], s[1]);
                if (pat != null)
                {
                    dbconn.Delete(String.Format("DELETE FROM Pacient where ID_pacient={0}", pat.ID_pacient));
                }
                lbox.Items.Clear();
                refresh();
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lbox.SelectedItem == null)
            { MessageBox.Show("Označte pacienta, kterého chcete upravit."); }
            else
            {
                UPDATE okno = new UPDATE();
                okno.jmeno.Text = pacienti[lbox.SelectedIndex].jmeno_p;
                okno.prijmeni.Text = pacienti[lbox.SelectedIndex].prijmeni_p;
                okno.vek.Text = pacienti[lbox.SelectedIndex].vek.ToString();
                okno.adresa.Text = pacienti[lbox.SelectedIndex].adresa_ID.ToString();
                okno.ShowDialog();
                //okno.pacient.ID_pacient = pacienti[lbox.SelectedIndex].ID_pacient;
                if (
                //okno.pacient == pacienti[lbox.SelectedIndex]) 
                okno.jmeno.Text == pacienti[lbox.SelectedIndex].jmeno_p &&
                okno.prijmeni.Text == pacienti[lbox.SelectedIndex].prijmeni_p &&
                okno.vek.Text == pacienti[lbox.SelectedIndex].vek.ToString() &&
                okno.adresa.Text == pacienti[lbox.SelectedIndex].adresa_ID.ToString())
                {
                    MessageBox.Show("Neprovedena žádná změna.");
                }
                else
                {
                    Patient novy = okno.pacient;
                    novy.ID_pacient = pacienti[lbox.SelectedIndex].ID_pacient;
                    pacienti[lbox.SelectedIndex] = novy;
                    lbox.Items[lbox.SelectedIndex] = pacienti[lbox.SelectedIndex].jmeno_p + " " + pacienti[lbox.SelectedIndex].prijmeni_p;
                    try
                    {
                        dbconn.Update(String.Format("UPDATE `Pacient` SET `jmeno_p` = '{0}', `prijmeni_p` = '{1}', `vek` = '{2}', `adresa_ID` = '{3}' WHERE `Pacient`.`ID_pacient` = {4};", novy.jmeno_p, novy.prijmeni_p, novy.vek, novy.adresa_ID, novy.ID_pacient));
                    }
                    catch (MySqlConnector.MySqlException)
                    { MessageBox.Show("Zadana adresa neexistuje."); }
                    lbox.Items.Clear();
                    refresh();
                }
                
            } 
        }
        public Patient getByName(string jmeno, string prijmeni)
        {
            foreach (var o in pacienti)
            {
                if (o.jmeno_p == jmeno && o.prijmeni_p == prijmeni)
                {
                    return o;
                }
            }
            return null;
        }

        public void refresh()
        {
            if(pacienti.Count>0)
            { pacienti.Clear(); }
            string sel = "SELECT * FROM Pacient";
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                pacienti.Add(new Patient(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetInt32(3),
                    reader.GetInt32(4)
                    ));
            }
            this.dbconn.CloseConn();
            
            foreach (var o in this.pacienti)
            {
                lbox.Items.Add(String.Format("{0} {1}", o.jmeno_p, o.prijmeni_p));
            }

        }
    }
}
