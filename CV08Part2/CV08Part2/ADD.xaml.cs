﻿using CV08Part1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CV08Part2
{
    /// <summary>
    /// Interaction logic for ADD.xaml
    /// </summary>
    public partial class ADD : Window
    {
        public Patient pacient { get; set; }
        int id = 1;
        public ADD()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            this.pacient = new Patient(id,jmeno.Text, prijmeni.Text, Int32.Parse(vek.Text), Int32.Parse(adresa.Text));
            Close();
        }
    }
}
