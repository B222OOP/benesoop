﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV08Part1
{
    public class Patient
    {
        public int ID_pacient { get; set; }
        public string jmeno_p { get; set; }
        public string prijmeni_p { get;  set; }
        public int vek { get;  set; }
        public int adresa_ID { get;  set; }
        public Patient(int ID_pacient, string jmeno_p, string prijmeni_p, int vek, int adresa_ID)
        {
            this.ID_pacient = ID_pacient;
            this.jmeno_p = jmeno_p;
            this.prijmeni_p = prijmeni_p;
            this.vek = vek;
            this.adresa_ID = adresa_ID;
        }
        public bool idControl(int id)
        {
            if (ID_pacient == id)
            {
                return true;
            }
            return false;
        }

        public string toString()
        {
           return String.Format($"Jmeno:{jmeno_p} Prijmeni:{prijmeni_p} Vek:{vek}");
        }
    }
}
