﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV06
{
    public class Zvire
    {
        public string jmeno { get; set; }
        public int vek { get; set; }
        public int pocet_nohou { get; set; }

        public virtual string barva { get ; set; }

        public Zvire(string jmeno, int vek, int pocet_nohou, string barva)
        {
            this.jmeno = jmeno;
            this.vek = vek;
            this.pocet_nohou = pocet_nohou;
            this.barva = barva;
        }
        public virtual string rev()
        {
            return "Takhle rve zvire:";
        }
        public string getName()
        {
            return jmeno;
        }
        private int getAge()
        {
            return vek;
        }
        protected int getNumOfLegs()
        {
            return pocet_nohou;
        }
    }
}
