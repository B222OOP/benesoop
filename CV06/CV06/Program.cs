﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV06
{
    class Program
    {
        static void Main(string[] args)
        {
            Pes pejsek = new Pes("Maxik", 5, 4, "vlk","Zlaty");
            Console.WriteLine(pejsek.rev());
            Console.WriteLine(pejsek.jmeno);
            //pejsek.tryMethode();
            Console.WriteLine(pejsek.barva+"\n");

            Zvire animal = new Zvire("Zviratko", 99, 10, "Zelena");
            Console.WriteLine(animal.rev());
            Console.WriteLine(animal.jmeno);
            Console.WriteLine(animal.barva);

            Console.ReadKey();
        }
    }
}
