﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV06
{
    public class Pes : Zvire
    {

        public string rasa;
        public override string barva { get => base.barva + ("Overrided"); set => base.barva = value; }
        public Pes(string jmeno, int vek, int pocet_nohou, string rasa,string barva):base(jmeno, vek,pocet_nohou,barva)
        {
            this.rasa = rasa;
        }
        public void tryMethode()
        {
            Console.WriteLine(getName());
            //Console.WriteLine(getAge()); nelze
            Console.WriteLine(getNumOfLegs());
        }
        public override string rev()
        {
            return "Takhle steka pejsek";
        }


    }
}
