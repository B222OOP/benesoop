﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diar
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // instance diáře
            Diar diar = new Diar();
            char volba = '0';
            // hlavní cyklus
            while (volba != '5')
            {
                diar.VypisUvodniObrazovku();
                Console.WriteLine();
                Console.WriteLine("Vyberte si akci:");
                Console.WriteLine("1 - Přidat záznam");
                Console.WriteLine("2 - Vypsat záznamy");
                Console.WriteLine("3 - Vyhledat záznamy");
                Console.WriteLine("4 - Vymazat záznam");
                Console.WriteLine("5 - Konec");
                volba = Console.ReadKey().KeyChar;
                Console.WriteLine();
                // reakce na volbu
                switch (volba)
                {
                    case '1':
                        diar.PridejZaznam();
                        break;
                    case '2':
                        diar.UkazZaznamy();
                        break;
                    case '3':
                        diar.VyhledejZaznamy();
                        break;
                    case '4':
                        diar.VymazZaznamy();
                        break;
                    case '5':
                        Console.WriteLine("Libovolnou klávesou ukončíte program...");
                        break;
                    default:
                        Console.WriteLine("Neplatná volba, stiskněte libovolnou klávesu a opakujte volbu.");
                        break;
                }
                Console.ReadKey();
            }
        }
    }
}
