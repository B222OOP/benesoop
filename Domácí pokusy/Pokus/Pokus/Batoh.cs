﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Pokus
{
    internal class Batoh
    {
        public List<Zaznam> predmety = new List<Zaznam>();
        public void PridejPredmet(string predmet, int pocet, int vaha)
        {
            predmety.Add(new Zaznam(predmet, pocet, vaha));
        }
        public List<Zaznam> NajdiPredmety(string vec)
        {
            List<Zaznam> nalezene = new List<Zaznam>();
            foreach (Zaznam z in predmety)
            {
                if (z.predmet == vec)
                    nalezene.Add(z);
            }
            return nalezene;
        }


        public void VymazPredmet(string vec)
        {
            List<Zaznam> nalezeno = NajdiPredmety(vec);
            foreach (Zaznam z in nalezeno)
                predmety.Remove(z);
        }
    }
}
