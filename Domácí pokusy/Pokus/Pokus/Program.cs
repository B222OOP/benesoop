﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokus
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // instance diáře
            Akce akce = new Akce();
            char volba = '0';
            // hlavní cyklus
            while (volba != '5')
            {
                akce.VypisUvodniObrazovku();
                Console.WriteLine();
                Console.WriteLine("Vyberte si akci:");
                Console.WriteLine("1 - Přidat předmet");
                Console.WriteLine("2 - Vypsat předmety");
                Console.WriteLine("3 - Vyhledat předmet");
                Console.WriteLine("4 - Vyndat předmet");
                Console.WriteLine("5 - Konec");
                volba = Console.ReadKey().KeyChar;
                Console.WriteLine();
                // reakce na volbu
                switch (volba)
                {
                    case '1':
                        akce.PridejPredmet();
                        break;
                    case '2':
                        akce.UkazPredmety();
                        break;
                    case '3':
                        akce.VyhledejPredmet();
                        break;
                    case '4':
                        akce.VyhodPredmet();
                        break;
                    case '5':
                        Console.WriteLine("Libovolnou klávesou ukončíte program...");
                        break;
                    default:
                        Console.WriteLine("Neplatná volba, stiskněte libovolnou klávesu a opakujte volbu.");
                        break;
                }
                Console.ReadKey();

            }
        }
    }
 
}
