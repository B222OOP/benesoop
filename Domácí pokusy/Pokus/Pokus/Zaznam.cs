﻿namespace Pokus
{
    public class Zaznam
    {
        public string predmet { get; set; }
        public int pocet { get; set; }
        public int vaha { get; set; }

        public Zaznam(string predmet, int pocet, int vaha)
        {
            this.predmet = predmet;
            this.pocet = pocet;
            this.vaha = vaha;
        }
        public override string ToString()
        {
            return this.predmet + " " + this.pocet + "ks " + this.vaha + "kg";
        }
    }
}
