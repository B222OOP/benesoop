﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokus
{
    internal class Akce
    {
        private Batoh batoh;

        public Akce()
        {
            batoh = new Batoh();
        }
        /*Možné zjednodušení v budoucnu:
        public void ZjistiPredmet()
        {
            Console.WriteLine("Zadejte nazev predmetu: ");
            string predmet;
            while (string.IsNullOrWhiteSpace(predmet = Console.ReadLine()))
            {
                Console.WriteLine("Zadej nazev znovu:");
            }
            Console.WriteLine("Zadejte pocet kusu predmetu: ");
            int pocet = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Zadejte vahu predmetu v kg ");
            int vaha = Convert.ToInt32(Console.ReadLine());
        }*/
        public void VypisPredmety(string vec)
        {
            List<Zaznam> predmety = batoh.NajdiPredmety(vec);
            foreach (Zaznam z in predmety)
                Console.WriteLine(z);
        }
        public void UkazPredmety()
        {
            if (batoh.predmety.Count() > 0)
            {
                Console.WriteLine("Nalezeny tyto předměty: ");
                foreach (Zaznam z in batoh.predmety)
                    Console.WriteLine(z);
            }
            else
                // Nenalezeno
                Console.WriteLine("Batoh neobsahuje žádné předměty.");
        }
        public void PridejPredmet()
        {
            Console.WriteLine("Zadejte název předmětu: ");
            string predmet;
            while (string.IsNullOrWhiteSpace(predmet = Console.ReadLine()))
            {
                Console.WriteLine("Zadej název znovu:");
            }
            List<Zaznam> predmety = batoh.NajdiPredmety(predmet);
            while (predmety.Count() > 0)
            {
                Console.WriteLine("V batohu už máš jeden takový předmět.");
                Console.WriteLine("Zvol si jiný předmět:");
                while (string.IsNullOrWhiteSpace(predmet = Console.ReadLine()))
                {
                    Console.WriteLine("Zadej název znovu:");
                }
                List<Zaznam> predmety2 = batoh.NajdiPredmety(predmet);
                predmety = predmety2;
            }
            Console.WriteLine("Zadejte pocet kusu predmetu: ");
            int pocet = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Zadejte váhu předmětu v kg:");
            int vaha = Convert.ToInt32(Console.ReadLine());
            batoh.PridejPredmet(predmet, pocet, vaha);
        }
        public void VyhledejPredmet()
        {
            // Zadání data uživatelem
            Console.WriteLine("Zadejte hledaný předmět:");
            string str = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(str))
            {
                Console.WriteLine("Zadej název znovu:");
            }
                // Vyhledání záznamů
                List<Zaznam> predmety = batoh.NajdiPredmety(str);
            // Výpis záznamů
            if (predmety.Count() > 0)
            {
                Console.WriteLine("Nalezeny tyto předměty: ");
                foreach (Zaznam z in predmety)
                    Console.WriteLine(z);
            }
            else
                // Nenalezeno
                Console.WriteLine("Nebyly nalezeny žádné předměty.");
        }
        public void VyhodPredmet()
        {
            Console.WriteLine("Zadejte název předmětu: ");
            string predmet;
            while (string.IsNullOrWhiteSpace(predmet = Console.ReadLine()))
            {
                Console.WriteLine("Zadej název znovu:");
            }
            Console.WriteLine("Z batohu budou odebrány předměty.");
            batoh.VymazPredmet(predmet);
        }
        public void VypisUvodniObrazovku()
        {
            Console.Clear();
            Console.WriteLine("Vítejte rozhraní batohu!");
            Console.WriteLine("Batoh obsahuje {0} předmětů.", batoh.predmety.Count);
            Console.WriteLine();    
        }
    }
}
