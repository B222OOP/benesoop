﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV03
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Fighter> listbojovniku = new List<Fighter>();
            Fighter fighter1 = new Fighter("Marcel", 17, 6, 120, 20);
            Fighter fighter2 = new Fighter("Karel", 11, 14, 145, 8);
            listbojovniku.Add(fighter1);
            listbojovniku.Add(fighter2);
            listbojovniku.Add(new Fighter("Petr", 14, 9, 120, 14));
            listbojovniku.Add(new Fighter("Pavel", 14, 9, 120, 14));
            listbojovniku.Add(new Fighter("Antonin", 14, 9, 120, 14));
            listbojovniku.Add(new Fighter("Tonda", 14, 9, 120, 14));
            listbojovniku.Add(new Fighter("Jozef", 14, 9, 120, 14));
            listbojovniku.Add(new Fighter("Pepik", 14, 9, 120, 14));
            Tournament turnaj = new Tournament(listbojovniku);
            turnaj.start();

            Console.ReadKey();
        }
    }
}
