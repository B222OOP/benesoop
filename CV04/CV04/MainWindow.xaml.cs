﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CV04
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        //int i = 1;
        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            //i.ToString();
            //tb1.Text = $"{i}";
            
            lbl.Content += tb1.Text + ", ";
            tb1.Text = (Int32.Parse(tb1.Text) + 1).ToString();
            
            //lbl.Content = tb1.Text;
            /*
            lbl.Content = "Ahoj svete";
            if (lbl.Content.ToString() == "Ahoj svete")
            {
                lbl.Content = "Nazdar";
            }
            else
            {
                lbl.Content = "Ahoj svete";
            }
            */
        }

        private void plus_Click(object sender, RoutedEventArgs e)
        {
            tb2.Text = ((Int32.Parse(tb2.Text) + Int32.Parse(tb1.Text))).ToString();
            tb1.Text = "";
        }

        private void minus_Click(object sender, RoutedEventArgs e)
        {
            tb2.Text = ((Int32.Parse(tb2.Text) - Int32.Parse(tb1.Text))).ToString();
            tb1.Text = "";
        }

        private void nasobeni_Click(object sender, RoutedEventArgs e)
        {
            tb2.Text = ((Int32.Parse(tb2.Text) * Int32.Parse(tb1.Text))).ToString();
            tb1.Text = "";
        }

        private void deleni_Click(object sender, RoutedEventArgs e)
        {
            tb2.Text = ((Int32.Parse(tb2.Text) / Int32.Parse(tb1.Text))).ToString();
            tb1.Text = "";
        }

        private void modulo_Click(object sender, RoutedEventArgs e)
        {
            tb2.Text = ((Int32.Parse(tb2.Text) % Int32.Parse(tb1.Text))).ToString();
            tb1.Text = "";
        }

        private void rovno_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = "";
        }
    }
}
