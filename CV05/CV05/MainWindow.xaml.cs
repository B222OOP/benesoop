﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CV05
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Point position;
        private List<Rectangle> rectangleList = new List<Rectangle>();
        private List<Point> pointList = new List<Point>();
        private Rectangle rct;
        private DispatcherTimer timer;
        private int horizontalne = 1;
        private int vertikalne = 1;
        public MainWindow()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(dispatcherTimer_Tick);
            timer.Interval = TimeSpan.FromMilliseconds(10);
            //addRectangles(1);
        }
        public void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if ((this.position.X + this.rct.Width + 5 > cnv.Width) || (this.position.X - 5 < 0))
            {
                this.horizontalne *= -1;
            }
            if ((this.position.Y + this.rct.Height + 5 > cnv.Height) || (this.position.Y - 5 < 0))
            {
                this.vertikalne *= -1;
            }
            this.position.X += (5 * horizontalne);
            this.position.Y += (5 * vertikalne);

            Canvas.SetLeft(this.rct, this.position.X);
            Canvas.SetTop(this.rct, this.position.Y);
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            addRectangle();
            timer.Start();
        }

        public void addRectangle()
        {
            //vytvoreni
            rct = new Rectangle();
            rct.Fill = new SolidColorBrush(Colors.Blue);
            rct.Width = 40;
            rct.Height = 20;
            Random rnd = new Random();
            int x = rnd.Next(0, (int)cnv.Width-40);
            int y = rnd.Next(0, (int)cnv.Height-20);

            this.position = new Point();
            this.position.X = x;
            this.position.Y = y;
            

            //pozice v canvasu
            Canvas.SetLeft(rct, this.position.X);
            Canvas.SetTop(rct, this.position.Y);
            //zarazeni do canvasu
            cnv.Children.Add(rct);

        }
        private void addRectangles(int n)
        {
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                int[] Xvalues;
                int[] Yvalues;
                Rectangle rct = new Rectangle();
                rct.Fill = new SolidColorBrush(Color.FromRgb((byte)rnd.Next(1, 255), (byte)rnd.Next(1, 255), (byte)rnd.Next(1, 233)));
                rct.Height = rnd.Next(10, 10 + rnd.Next(10, 100));
                rct.Width = rnd.Next(10, 10 + rnd.Next(10, 100));
                while (true)
                {
                    Xvalues = assignArray(rnd.Next(0, (int)cnv.Width), rnd.Next(0, (int)cnv.Width));
                    Yvalues = assignArray(rnd.Next(0, (int)cnv.Height), rnd.Next(0, (int)cnv.Height));
                    int val = 0;
                    for (int j = 0; i < this.pointList.Count; j++)
                    {
                        if (!(Xvalues[0] <= pointList[i].X && pointList[i].X <= Xvalues[Xvalues.Length]))
                        {
                            if (!(Yvalues[0] <= pointList[i].Y && pointList[i].Y <= Xvalues[Yvalues.Length]))
                            {
                                val += 1;
                                continue;
                            }
                        }

                    }

                    if (val == this.pointList.Count) { break; }
                }
                Point p = new Point();
                p.X = Xvalues[0];
                p.Y = Yvalues[0];
                this.rectangleList.Add(rct);
                this.pointList.Add(p);
                Canvas.SetLeft(rct, Xvalues[0]);
                Canvas.SetTop(rct, Yvalues[0]);
                cnv.Children.Add(rct);

            }

        }

        private int[] assignArray(int min, int max)
        {
            int[] array;
            if (min > max)
            {
                array = new int[min - max];
            }
            else
            {
                array = new int[max - min];
            }
            for (int i = 0; i < array.Length; i++)
            {
                if (i == 0)
                {
                    array[i] = min;
                }
                else
                {
                    array[i] = array[i - 1] + 1;
                }

            }
            return array;
        }
    }
}

