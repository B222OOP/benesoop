﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace Klinika2
{
    public class Pacient 
    {
        public string jmeno { get; private set; }
        public string prijmeni { get; private set; }
        public string rc { get; private set; }
        public int hmotnost { get; private set; }
        public int vyska { get; private set; }


        public Pacient(string jmeno, string prijmeni, string rc, int hmotnost, int vyska)

        {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.rc = rc;
            this.hmotnost = hmotnost;
            this.vyska = vyska;
        }  
        
    }
}
