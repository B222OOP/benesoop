﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Pacient> pacienti = new List<Pacient>();
        List<Pokoj> pokoje = new List<Pokoj>();
        List<Postel> postele = new List<Postel>();
        public MainWindow()
        {
            InitializeComponent();
        }
        
        int j = 1;
        private void reloadListBox()
        {                    
                string jmeno = "Pokoj " + j;
                if (!lboxPokoje.Items.Contains(jmeno)) { lboxPokoje.Items.Add(jmeno); }
                j++;
                
        }
        
        public void pokoj_Click(object sender, RoutedEventArgs e)
        {
           
            Okno_pokoje okno = new Okno_pokoje();
            okno.ShowDialog();
            Pokoj novy = okno.pokoj;
            pokoje.Add(novy);
            reloadListBox();
            
        }

        public void Vypis_posteli(object sender, SelectionChangedEventArgs args)
        { 
            lboxPostele.Items.Clear();

            for (int i = 0; i < pokoje[lboxPokoje.SelectedIndex].pocetLuzek; i++)
            {
                postele.Add(new Postel("L" + (i + 1), pokoje[lboxPokoje.SelectedIndex].maxHmotnostLuzek)); 
                    lboxPostele.Items.Add(postele[i].jmeno);
                

            }

            postele.Clear();
            lboxPostele.Visibility = Visibility.Visible;
        }


        public void pacient_Click(object sender, RoutedEventArgs e)
        {
            Okno_pacientu okno = new Okno_pacientu();
            okno.ShowDialog();
            Pacient novy = okno.pacient;
            pacienti.Add(novy); 
            if (!lboxPacienti.Items.Contains(novy.jmeno + " " + novy.prijmeni))
            { lboxPacienti.Items.Add(novy.jmeno + " " + novy.prijmeni);
              
            }
            else MessageBox.Show("Pacient již existuje. Zkuste zadat pacienta znovu.");
            


        }
        private List<Pacient> NajdiPacienta(string jmeno, string prijmeni)
        {
            List<Pacient> nalezen = new List<Pacient>();
            foreach (Pacient p in pacienti)
            {
                if (p.jmeno == jmeno && p.prijmeni == prijmeni)
                    nalezen.Add(p);
                else MessageBox.Show("Pacient neexistuje. Zadejte jiné jméno.");
                break;
            }
            return nalezen;
        }

        public void postel_Click(object sender, RoutedEventArgs e)
        {
            Okno_pridani okno = new Okno_pridani();
            okno.ShowDialog();
            List<Pacient> pac = NajdiPacienta(okno.txt_jmeno.Text, okno.txt_prijmeni.Text);
            int a = Int32.Parse(okno.txt_luzko.Text);
            int b = Int32.Parse(okno.txt_pokoj.Text) - 1;
            pokoje[b].pridej(pac[0], a);

            lboxPostele.Items.Clear();
            for (int i = 1; i <= pokoje[b].pocetLuzek; i++)
            {
                postele.Add(new Postel("L" + i, pokoje[b].maxHmotnostLuzek));
                if (i == a && pac[0].hmotnost<= postele[a - 1].maxHmotnost)
                    lboxPostele.Items.Add(postele[a-1].jmeno + "     " + pac[0].jmeno + " " + pac[0].prijmeni);
                else
                    lboxPostele.Items.Add(postele[i - 1].jmeno);
            }
            
            

        }
    }
}
