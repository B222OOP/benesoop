﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika2
{
    /// <summary>
    /// Interaction logic for Okno_pacientu.xaml
    /// </summary>
    public partial class Okno_pacientu : Window
    {
        public Pacient pacient { get; set; }
        public Okno_pacientu()
        {
            InitializeComponent();
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            this.pacient = new Pacient(txt_jmeno.Text, txt_prijmeni.Text, txt_rc.Text, Int32.Parse(txt_vaha.Text), Int32.Parse(txt_vyska.Text));
            Close();
        }
    }
}
