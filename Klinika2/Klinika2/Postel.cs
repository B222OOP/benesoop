﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Klinika2
{
    public class Postel
    {
        

            public string jmeno { get; private set; }
            public int maxHmotnost { get; private set; }
            public bool obsazeno { get; set; }
            public string obsadil { get; set; }

            public Postel(string jmeno, int maxHmotnost)
            {
                this.jmeno = jmeno;
                this.maxHmotnost = maxHmotnost;
                this.obsazeno = false;
                this.obsadil = "";
            }
    }
}
