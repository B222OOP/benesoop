﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace Klinika2
{
    public class Pokoj : MainWindow
    {
        public int pocetLuzek { get; private set; }
        public int maxHmotnostLuzek { get; private set; }

        public List<Postel> postele = new List<Postel>();
        public List<Pacient> pacienti = new List<Pacient>();

        public Pokoj(int pocetLuzek, int maxHmotnostLuzek)
        {
            this.pocetLuzek = pocetLuzek;
            this.maxHmotnostLuzek = maxHmotnostLuzek;
            
            for (int i = 0; i < pocetLuzek; i++)
            {
                postele.Add(new Postel( "L" + i, maxHmotnostLuzek));
            }
            
        }
        
        public void pridej(Pacient pac, int i)
        {
            i -= 1;
            if (postele[i].obsazeno == false)
            {
                if (pac.hmotnost <= maxHmotnostLuzek)
                {
                    pacienti.Add(pac);
                    postele[i].obsazeno = true;
                    postele[i].obsadil = pac.jmeno + " " + pac.prijmeni;
                }
                else
                {
                    MessageBox.Show("Pacientova hmotnost je příliš velká.");
                    return;
                }
            }
            else
            {
                MessageBox.Show($"Lůžko {postele[i].jmeno} již obsadil {postele[i].obsadil}.");
                return;
            }
        }
    }
       
}
