﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika2
{
    /// <summary>
    /// Interaction logic for Okno_pridani.xaml
    /// </summary>
    public partial class Okno_pridani : Window
    {
        public Okno_pridani()
        {
            InitializeComponent();
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
