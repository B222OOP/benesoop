﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika2
{
    /// <summary>
    /// Interaction logic for Okno_pokoje.xaml
    /// </summary>
    
    public partial class Okno_pokoje : Window
    {
        public Pokoj pokoj { get; set; }
        
        public Okno_pokoje()
        {
            InitializeComponent();
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
           this.pokoj = new Pokoj(Int32.Parse(txt_pocet.Text), Int32.Parse(txt_hmotnost.Text));
           
           this.Close();
        }
    }
}
