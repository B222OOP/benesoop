﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika
{
    internal class Klinika
    {
        public List<Patient> pacienti = new List<Patient>();
        public void prijmiPacienta(string jmeno, string prijmeni, string rc, int hmotnost, int vyska)
        {
            pacienti.Add(new Patient(jmeno, prijmeni, rc, hmotnost, vyska, true));
        }
        public List<Patient> NajdiPacienta(string jmeno, string prijmeni)
        {
            List<Patient> nalezen = new List<Patient>();
            foreach (Patient p in pacienti)
            {
                if (p.jmeno == jmeno && p.prijmeni == prijmeni)
                    nalezen.Add(p);
            }
            return nalezen;
        }
        public void VymazPacienta(string jmeno, string prijmeni)
        {
            List<Patient> nalezen = NajdiPacienta(jmeno, prijmeni);
            foreach (Patient p in nalezen)
                pacienti.Remove(p);
        }
    }
}
