﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika
{
    internal class Akce
    {
        private Klinika room;

        public Akce()
        {
            room = new Klinika();
        }
       
        public void UkazPacienty()
        {
            if (room.pacienti.Count() > 0)
            {
                Console.WriteLine("Na klinice jsou tito pacienti: ");
                foreach (Patient p in room.pacienti)
                    p.vypis();
            }
            else
                // Nenalezeno
                Console.WriteLine("Na klinice nejsou přijati žádní pacienti.");
        }
        public string nacti()
        {
            string str;
            while (string.IsNullOrWhiteSpace(str = Console.ReadLine()))
            {
                Console.WriteLine("Chyba, prosím zadejte znovu:");
            }
            return str;
        }
    public void PridejPacienta()
        {
            Console.WriteLine("Zadejte jméno pacienta: ");
            string jmeno = nacti();
            Console.WriteLine("Zadejte příjmení pacienta: ");
            string prijmeni = nacti();
            Console.WriteLine("Zadejte rodné číslo pacienta: ");
            string rc = nacti();
            Console.WriteLine("Zadejte hmotnost pacienta v kg: ");
            int hmotnost = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Zadejte výšku pacienta v cm:");
            int vyska = Convert.ToInt32(Console.ReadLine());
            room.prijmiPacienta(jmeno, prijmeni, rc, hmotnost, vyska);
        }
        public void VyhledejPacienta()
        {
            Console.WriteLine("Zadejte jméno hledaného pacienta:");
            string jmeno = nacti();
            Console.WriteLine("Zadejte příjmení pacienta: ");
            string prijmeni = nacti(); 
            List<Patient> pacienti = room.NajdiPacienta(jmeno,prijmeni);
            if (pacienti.Count() > 0)
            {
                Console.WriteLine("Nalezeni pacienti: ");
                foreach (Patient p in pacienti)
                    p.vypis();
            }
            else
                // Nenalezeno
                Console.WriteLine("Na klinice nebyl přijat žádný pacient s tímto jménem.");
        }
        public void PropustPacienta()
        {
            Console.WriteLine("Zadejte jméno hledaného pacienta:");
            string jmeno = nacti();
            Console.WriteLine("Zadejte příjmení pacienta: ");
            string prijmeni = nacti();
            Console.WriteLine($"Z kliniky byl propuštěn pacient {jmeno} {prijmeni}.");
            room.VymazPacienta(jmeno, prijmeni);
        }
    }
}
