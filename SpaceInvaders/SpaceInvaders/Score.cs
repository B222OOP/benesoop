﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceInvaders
{
    public class Score
    {
        public int ID { get; set; }
        public string name { get; set; }
        public int score { get; set; }
   
        public Score(int ID, string name, int score)
        {
            this.ID = ID;
            this.name = name;
            this.score = score;
        }
    }
}
