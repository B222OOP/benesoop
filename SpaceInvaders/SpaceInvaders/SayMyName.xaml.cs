﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SpaceInvaders
{
    /// <summary>
    /// Interaction logic for SayMyName.xaml
    /// </summary>
    public partial class SayMyName : Window
    {
        public Score sc { get; set; }
        int id = 1;
        int score = 1;
        public SayMyName()
        {
            InitializeComponent();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (txt.Text == "")
            { 
                MessageBox.Show("Enter the name"); 
            }
            else
            {
                Close();
                this.sc = new Score(id, txt.Text, score);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (txt.Text == "")
            {
                MessageBox.Show("Enter the name");
            }
            else
            {
                Close();
                this.sc = new Score(id, txt.Text, score);
                Game subWindow = new Game();
                subWindow.ShowDialog();
            }
            
        }
    }
}
