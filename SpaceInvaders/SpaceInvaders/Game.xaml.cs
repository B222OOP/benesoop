﻿using CV08Part2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MySqlConnector;
using static System.Net.Mime.MediaTypeNames;

namespace SpaceInvaders
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Game : Window
    {

        bool goLeft, goRight = false;
        List<Rectangle> itemstoremove = new List<Rectangle>();
        int enemyImages = 0;
        int bulletTimer = 12;
        int bulletTimerLimit = 90;
        int totalEnemeis;
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        ImageBrush playerSkin = new ImageBrush();
        double enemySpeed = 5;
        int wins = 0;
        public int points = 0;
        int lives = 3;
        int smer = 1;
        int bullets = 0;
        private DB_connect dbconn;
        private MySqlDataReader reader;


        public Game()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();
            dispatcherTimer.Tick += gameEngine;
            dispatcherTimer.Interval = TimeSpan.FromMilliseconds(20);
            dispatcherTimer.Start();
            playerSkin.ImageSource = new BitmapImage(new Uri("C:\\Users\\amigo\\source\\repos\\benesoop1\\SpaceInvaders\\SpaceInvaders\\images\\defender.png"));
            player1.Fill = playerSkin;
            makeEnemies(5);
        }

        private void Canvas_KeyisDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                goLeft = true;
            }
            if (e.Key == Key.Right)
            {
                goRight = true;
            }
        }

        private void Canvas_KeyIsUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                goLeft = false;
            }
            if (e.Key == Key.Right)
            {
                goRight = false;
            }

            if (e.Key == Key.Space)
            {
                itemstoremove.Clear();
                Rectangle newBullet = new Rectangle
                {
                    Tag = "bullet",
                    Height = 20,
                    Width = 5,
                    Fill = Brushes.White,
                    Stroke = Brushes.Red
                };

                Canvas.SetTop(newBullet, Canvas.GetTop(player1) - newBullet.Height);
                Canvas.SetLeft(newBullet, Canvas.GetLeft(player1) + player1.Width / 2);
                myCanvas.Children.Add(newBullet);
            }
        }
        private void enemyBulletMaker(double x, double y)
        {
            Rectangle newEnemyBullet = new Rectangle
            {
                Tag = "enemyBullet",
                Height = 40,
                Width = 15,
                Fill = Brushes.Yellow,
                Stroke = Brushes.Black,
                StrokeThickness = 5

            };

            Canvas.SetTop(newEnemyBullet, y);
            Canvas.SetLeft(newEnemyBullet, x);
            myCanvas.Children.Add(newEnemyBullet);
        }

        public void makeEnemies(int limit)
        {
            int left = 0;
            int top = 30;
            totalEnemeis = limit;

            for (int i = 0; i < limit; i++)
            {

                ImageBrush enemySkin = new ImageBrush();
                Rectangle newEnemy = new Rectangle
                {
                    Tag = "enemy",
                    Height = 45,
                    Width = 45,
                    Fill = enemySkin,

                };

                Canvas.SetTop(newEnemy, top);
                Canvas.SetLeft(newEnemy, left);
                myCanvas.Children.Add(newEnemy);
                left -= 60;

                enemyImages++;
                if (enemyImages > 3)
                {
                    enemyImages = 1;

                }



                switch (enemyImages)
                {
                    case 1:
                        enemySkin.ImageSource = new BitmapImage(new Uri("C:\\Users\\amigo\\source\\repos\\benesoop1\\SpaceInvaders\\SpaceInvaders\\images\\enemy 1.png"));

                        break;
                    case 2:
                        enemySkin.ImageSource = new BitmapImage(new Uri("C:\\Users\\amigo\\source\\repos\\benesoop1\\SpaceInvaders\\SpaceInvaders\\images\\enemy 2.png"));

                        break;
                    case 3:
                        enemySkin.ImageSource = new BitmapImage(new Uri("C:\\Users\\amigo\\source\\repos\\benesoop1\\SpaceInvaders\\SpaceInvaders\\images\\enemy 3.png"));

                        break;

                }

            }
        }


        private void gameEngine(object sender, EventArgs e)
        {
            Rect player = new Rect(Canvas.GetLeft(player1), Canvas.GetTop(player1), player1.Width, player1.Height);
            score.Content = "Score: " + points;
            chances.Content = "Lives: " + lives;


            if (goLeft && Canvas.GetLeft(player1) > 10)
            {
                Canvas.SetLeft(player1, Canvas.GetLeft(player1) - 10);
            }
            else if (goRight && Canvas.GetLeft(player1) < 730)
            {
                Canvas.SetLeft(player1, Canvas.GetLeft(player1) + 10);
            }

            bulletTimer -= 3;

            if (bulletTimer < 0)
            {
                if (bullets < 2)
                {
                    Random rnd = new Random();
                    int num = rnd.Next(800);
                    enemyBulletMaker(num, 0);
                    bullets++;
                }
                else
                {
                    enemyBulletMaker(Canvas.GetLeft(player1) + 20, 0);
                    bullets = 0;
                }

                bulletTimer = bulletTimerLimit;
            }

            foreach (var x in myCanvas.Children.OfType<Rectangle>())
            {
                if (x is Rectangle && (string)x.Tag == "bullet")
                {
                    Canvas.SetTop(x, Canvas.GetTop(x) - 20);

                    Rect bullet = new Rect(Canvas.GetLeft(x), Canvas.GetTop(x), x.Width, x.Height);


                    if (Canvas.GetTop(x) < 10)
                    {
                        itemstoremove.Add(x);
                    }

                    foreach (var y in myCanvas.Children.OfType<Rectangle>())
                    {
                        if (y is Rectangle && (string)y.Tag == "enemy")
                        {
                            Rect enemy = new Rect(Canvas.GetLeft(y), Canvas.GetTop(y), y.Width, y.Height);

                            if (bullet.IntersectsWith(enemy))
                            {
                                itemstoremove.Add(x);
                                itemstoremove.Add(y);
                                totalEnemeis -= 1;
                                points += 10;
                                enemySpeed += 0.2;
                            }
                        }
                    }
                }

                if (x is Rectangle && (string)x.Tag == "enemy")
                {

                    Canvas.SetLeft(x, Canvas.GetLeft(x) + enemySpeed * smer);
                    if (Canvas.GetLeft(x) > 820)
                    {
                        Canvas.SetLeft(x, -80);

                        Canvas.SetTop(x, Canvas.GetTop(x) + (x.Height + 10));
                    }
                        Rect enemy = new Rect(Canvas.GetLeft(x), Canvas.GetTop(x), x.Width, x.Height);

                        if (player.IntersectsWith(enemy))
                        {
                            itemstoremove.Add(x);
                            lives--;
                            points += 10;
                            if (lives == 0)
                            {
                                dispatcherTimer.Stop();
                                MessageBox.Show("Game over");
                                Close();
                                SayMyName subWindow = new SayMyName();
                                subWindow.ShowDialog();
                                Score local = subWindow.sc;
                                string sel = "SELECT * FROM Leaderboard";
                                reader = this.dbconn.Select(sel);
                                while (reader.Read() && local.ID.Equals(reader.GetInt32(0)))
                                {
                                    local.ID++;
                                }

                                dbconn.Insert(String.Format("INSERT into `Leaderboard` (`ID`, `name`, `score`) VALUES ('{0}', '{1}', '{2}')", local.ID, local.name, points));
                            }
                        }
                    }



                    if (x is Rectangle && (string)x.Tag == "enemyBullet")
                    {

                        Canvas.SetTop(x, Canvas.GetTop(x) + 10);


                        if (Canvas.GetTop(x) > 480)
                        {
                            itemstoremove.Add(x);

                        }
                    


                        Rect enemyBullets = new Rect(Canvas.GetLeft(x), Canvas.GetTop(x), x.Width, x.Height);

                        if (enemyBullets.IntersectsWith(player))
                        {
                            itemstoremove.Add(x);
                            lives--;
                            if (lives == 0)
                            {
                                dispatcherTimer.Stop();
                                MessageBox.Show("Game over");
                                Close();
                                SayMyName subWindow = new SayMyName();
                                subWindow.ShowDialog();
                                Score local = subWindow.sc;
                                string sel = "SELECT * FROM Leaderboard";
                                reader = this.dbconn.Select(sel);
                                while (reader.Read() && local.ID.Equals(reader.GetInt32(0)))
                                {
                                    local.ID++;
                                }

                                dbconn.Insert(String.Format("INSERT into `Leaderboard` (`ID`, `name`, `score`) VALUES ('{0}', '{1}', '{2}')", local.ID, local.name, points));

                            } 
                        }
                    }
                }


                foreach (Rectangle y in itemstoremove)
                {
                    myCanvas.Children.Remove(y);
                }


                if (totalEnemeis < 1)
                {

                    MessageBox.Show("You survived this wave");
                    wins++;
                    makeEnemies(wins * 5 + 5);
                    goLeft = false;
                    goRight = false;

                }
            }
        }
    }

