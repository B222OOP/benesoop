﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;
using CV08Part2;
using MySqlConnector;

namespace SpaceInvaders
{
    /// <summary>
    /// Interaction logic for Leaderboard.xaml
    /// </summary>
    public partial class Leaderboard : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        public List<Score> score = new List<Score>();
        public Leaderboard()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();
            refresh();
            fill();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        public void refresh()
        {
            try
            {
                string sel = "SELECT * FROM Leaderboard ORDER BY score DESC";
                reader = this.dbconn.Select(sel);           
            while (reader.Read())
            {
                score.Add(new Score(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetInt32(2)
                    ));
            }
            this.dbconn.CloseConn();

            foreach (var o in this.score)
            {
                lbox.Items.Add(String.Format("{0}  ........................................................................................................  {1}", o.name, o.score));
            }
            }
            catch (MySqlConnector.MySqlException)
            { 
                MessageBox.Show("Zadana tabulka neexistuje.");    
            }

        }
        public void fill()
        {
            int items = lbox.Items.Count;
            string name = "........";
            string score = "........";
            for (int i = 0; i < 6 - items; i++)
            {
                lbox.Items.Add(String.Format("{0}  ........................................................................................................  {1}", name, score));
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            dbconn.Delete("DELETE FROM Leaderboard");
            refresh();
            lbox.Items.Clear();
            fill();
             
        }
    }
}
