﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV08Part1
{
    public class Patient
    {
        public string jmeno_p { get; private set; }
        public string prijmeni_p { get; private set; }
        public int vek { get; private set; }
        public int adresa_ID { get; private set; }
        public Patient(string jmeno_p, string prijmeni_p, int vek, int adresa_ID)
        {
            this.jmeno_p = jmeno_p;
            this.prijmeni_p = prijmeni_p;
            this.vek = vek;
            this.adresa_ID = adresa_ID;
        }
        public string toString()
        {
           return String.Format($"Jmeno:{jmeno_p} Prijmeni:{prijmeni_p} Vek:{vek}");
        }
    }
}
