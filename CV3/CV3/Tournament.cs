﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV3
{
    public class Tournament
    {
        public List<Fighter> listbojovniku { get; private set; }
        private List<List<Fight>> listSouboju = new List<List<Fight>>();

        public Tournament(List<Fighter> listbojovniku)
        {
            this.listbojovniku = listbojovniku;
        }

        public void start()
        {
            while (true)
            {
                List<Fight> tournamentTree = CreateTree(this.listbojovniku);
                for(int i=0;i<tournamentTree.Count;i++)
                {
                    tournamentTree[i].startFight();
                }
                saveTo2DList(tournamentTree);
                this.listbojovniku = GetWinners(tournamentTree);

                if (this.listbojovniku.Count == 1)
                {
                    Console.WriteLine("Vitezem se stal" + this.listbojovniku[0].jmeno);
                    break;
                }
            }

          
        }

        public List<Fight> CreateTree(List<Fighter> listbojovniku=null)
        {
            if (listbojovniku == null)
            {
                listbojovniku = this.listbojovniku;
            }
            List<Fight> listik = new List<Fight>();

            if (!isPowerOfTwo(listbojovniku.Count))
            {
                Console.WriteLine("Pocet bojovniku neni roven mocnine dvojky");
                //TODO:
            }
            for (int i = 0; i < listbojovniku.Count; i+=2)
            {
                listik.Add(new Fight(listbojovniku[i], listbojovniku[i + 1]));
            }
            return listik;        
        }

        public bool isPowerOfTwo(int n)
        {
            while (n % 2 == 0)
            {
                n /= 2;
            }
            if (n == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Fighter> GetWinners(List<Fight> listzapasu = null)
        {
            List<Fighter> vitezove = new List<Fighter>();
            //Fighter fighter;
            for(int i=0;i<listzapasu.Count;i++)
            {/*
                fighter = listzapasu[i].returnWinner();
                vitezove.Add(fighter);
                */
                vitezove.Add(listzapasu[i].returnWinner());
            }
            return vitezove;
        }

        public void saveTo2DList(List<Fight> souboje) 
        {
            this.listSouboju.Add(souboje);
        }


    }
}
